### Hey 👋, I'm boshieu

[![Github](https://img.shields.io/github/followers/bos-hieu?label=Follow&style=social)](https://gitlab.com/boshieu1994)

- 🔭 I’m currently working on Autonomous Inc as Software Enginner
- 🌱 I’m currently learning Golang

<a href="https://www.buymeacoffee.com/boshieu" target="_blank"><img src="https://cdn.buymeacoffee.com/buttons/default-orange.png" alt="Buy Me A Coffee" height="41" width="174"></a>

<a href="https://gitlab.com/boshieu1994">
  <img height="180em" src="https://gitlab-readme-stats.vercel.app/api?username=boshieu1994&show_icons=true&theme=merko&count_private=true" alt="Boshieu's gilab stats" />
  <img height="180em" src="https://gitlab-readme-stats.vercel.app/api/top-langs/?username=boshieu1994&theme=merko&layout=compact" alt="Boshieu's gitlab top languages" />
</a>
<br/>

<!--
**boshieu1994/boshieu1994** is a ✨ _special_ ✨ repository because its `README.md` (this file) appears on your Gitlab profile.

Here are some ideas to get you started:

- 🔭 I’m currently working on Autonomous Inc
- 🌱 I’m currently learning Software Architecture
- 👯 I’m looking to collaborate on ...
- 🤔 I’m looking for help with ...
- 💬 Ask me about ...
- 📫 How to reach me: ...
- 😄 Pronouns: ...
- ⚡ Fun fact: ...
-->